FROM tomcat:8.0-alpine

LABEL maintainer="sachin pal hi"

COPY ./target/*.war  /usr/local/tomcat/webapps/ROOT.war
EXPOSE 8090

CMD ["catalina.sh", "run"]
